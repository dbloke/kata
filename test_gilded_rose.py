# -*- coding: utf-8 -*-
import unittest

from gilded_rose import Item, GildedRose, OldCheeseItem, LegendaryItem, BackstagePassItem, ConjuredItem


class GildedRoseTest(unittest.TestCase):
    def test_item(self):
        items = [Item("basic test", 0, 0)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual("basic test", items[0].name)
        self.assertEqual(-1, items[0].sell_in)
        self.assertEqual(0, items[0].quality)

    def test_old_cheese_item(self):
        items = [OldCheeseItem("basic cheese test", 3, 0)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual("basic cheese test", items[0].name)
        self.assertEqual(2, items[0].sell_in)
        self.assertEqual(1, items[0].quality)
        for i in range(3):
            gilded_rose.update_quality()
        self.assertEqual(-1, items[0].sell_in)
        self.assertEqual(4, items[0].quality)

    def test_legendary_item(self):
        items = [LegendaryItem("basic legendary test", 0, 80)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual("basic legendary test", items[0].name)
        self.assertEqual(0, items[0].sell_in)
        self.assertEqual(80, items[0].quality)

    def test_backstage_pass_item(self):
        items = [BackstagePassItem("great concert", 12, 6)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual("great concert", items[0].name)
        self.assertEqual(11, items[0].sell_in)
        self.assertEqual(7, items[0].quality)

        gilded_rose.update_quality()
        gilded_rose.update_quality()
        self.assertEqual(9, items[0].sell_in)
        self.assertEqual(11, items[0].quality)

        for i in range(5):
            gilded_rose.update_quality()
        self.assertEqual(4, items[0].sell_in)
        self.assertEqual(23, items[0].quality)

        for i in range(4):
            gilded_rose.update_quality()

        self.assertEqual(0, items[0].sell_in)
        self.assertEqual(35, items[0].quality)

        gilded_rose.update_quality()
        self.assertEqual(-1, items[0].sell_in)
        self.assertEqual(0, items[0].quality)

    def test_conjured_item(self):
        items = [ConjuredItem("basic conjured test", 5, 5)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual("basic conjured test", items[0].name)
        self.assertEqual(4, items[0].sell_in)
        self.assertEqual(3, items[0].quality)

    def test_list(self):
        items = self.get_item_list(5, 60)
        for i in items:
            if not isinstance(i, LegendaryItem):
                self.assertEqual(50, i.quality)
            else:
                self.assertEqual(80, i.quality)

        gilded_rose = GildedRose(items)
        for i in range(55):
            gilded_rose.update_quality()

        for i in items:
            if not isinstance(i, LegendaryItem):
                self.assertGreaterEqual(50, i.quality)
                self.assertLessEqual(0, i.quality)
                self.assertEqual(-50, i.sell_in)
            else:
                self.assertEqual(80, i.quality)

    def get_item_list(self, sell_in, quality):
        items = [
            Item("list item", sell_in, quality),
            OldCheeseItem("list cheese", sell_in, quality),
            LegendaryItem("list legendary", sell_in, 80),
            BackstagePassItem("list backstage", sell_in, quality),
            ConjuredItem("list conjured", sell_in, quality)
        ]
        return items


if __name__ == '__main__':
    unittest.main()
