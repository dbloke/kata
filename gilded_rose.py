# -*- coding: utf-8 -*-


class GildedRose(object):

    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            item.update_quality()


class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = max(0, min(50, quality))

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)

    def update_quality(self):
        self.sell_in -= 1
        self.quality = max(0, min(50, self.quality - self.get_quality_loss()))

    def get_quality_loss(self):
        if self.sell_in < 0:
            return 2
        return 1


class OldCheeseItem(Item):
    def get_quality_loss(self):
        return -1


class LegendaryItem(Item):
    def __init__(self, name, sell_in, quality):
        Item.__init__(self, name, sell_in, quality)
        # The quality might be bigger that 50
        self.quality = quality

    def update_quality(self):
        pass

    def __repr__(self):
        return "%s, Legendary item, %s" % (self.name, self.quality)


class BackstagePassItem(Item):
    def update_quality(self):
        if self.sell_in > 0:
            Item.update_quality(self)
        else:
            self.sell_in -= 1
            self.quality = 0

    def get_quality_loss(self):
        if self.sell_in > 10:
            return -1
        elif self.sell_in > 5:
            return -2
        elif self.sell_in >= 0:
            return -3
        else:
            return 0


class ConjuredItem(Item):
    def get_quality_loss(self):
        return 2 * Item.get_quality_loss(self)
